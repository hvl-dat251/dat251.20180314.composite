package dat251.composite.solution;

import java.util.ArrayList;
import java.util.List;

public class ComplexShape implements Shape {

	List<Shape> componentShapes = new ArrayList<Shape>();

	@Override
	public void draw() {
		System.out.print("(A complex shape composed of: ");
		int i;
		int size = componentShapes.size();
		for (i=0; i<size-2; i++) {
			componentShapes.get(i).draw();
			System.out.print(", ");
		}
		if (size>0)
			componentShapes.get(i).draw();
		if (size>1) {
			System.out.print(" and ");
			componentShapes.get(++i).draw();
		}
		if (size>0)
			System.out.print(")");
	}


	public void add (Shape shape) {
		componentShapes.add(shape);
	}


}
