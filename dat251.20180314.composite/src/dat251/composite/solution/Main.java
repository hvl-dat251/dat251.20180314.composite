package dat251.composite.solution;

public class Main {

	public static void main(String[] args) {
		Rectangle r1 = new Rectangle();
		Rectangle r2 = new Rectangle();
		Circle c1 = new Circle();
		Circle c2 = new Circle();
		Circle c3 = new Circle();
		ComplexShape cs1 = new ComplexShape();
		ComplexShape cs2 = new ComplexShape();
		ComplexShape cs3 = new ComplexShape();
		
		cs1.add(r1);
		cs1.add(c1);
		
		cs2.add(cs3);
		cs2.add(r2);
		cs2.add(c2);
		
		cs3.add(c3);
		
		cs1.draw();
		System.out.println();
		cs2.draw();
	}

}
